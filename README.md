### applied patches:
* [fullgaps](https://dwm.suckless.org/patches/fullgaps/)
* [status2d](https://dwm.suckless.org/patches/status2d/)
* [actualfullscreen](https://dwm.suckless.org/patches/actualfullscreen/)(i think?)
* [stacker](https://dwm.suckless.org/patches/stacker/)

### disclaimer
**when i was creating this repo i realised that i didn't keep track of what patches i actually applied to this build, so this list might be missing some entries, and some entries might not actually be applied. At some point i'm going to actually check whether this list is accurate or not, until then you'll have to deal with this. godspeed!**

